# -*- coding: utf-8 -*-
import ConfigParser
import argparse

from readability import Readability


def read_html():
    config = ConfigParser.ConfigParser()
    try:
        config.readfp(open('settings.ini'))
    except IOError:
        config.readfp(open('default.ini'))
    usage = "python main.py http://example.com/"
    description = "Parse content from url"
    parser = argparse.ArgumentParser(
        add_help=True, usage=usage, description=description)
    parser.add_argument('url', action="store", help="url for parse content")
    if parser.parse_args().url:
        read = Readability(config, parser.parse_args().url)
        filename = read.convert()
        if filename:
            print "{0} - file saved".format(read.convert())
    else:
        print "use: python main.py -h"


if __name__ == '__main__':
    read_html()
