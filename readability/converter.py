# -*- coding: utf-8 -*-
from lxml.html import fromstring

from abstract import AbstractClass


class HtmlConverter(AbstractClass):
    html_content = None
    clear_content = None

    def __init__(self, html_content, config):
        self.set_html_content(html_content)
        self.set_config(config)
        self.parse()

    def get_clear_content(self):
        return self.clear_content

    def set_html_content(self, html_content):
        self.html_content = html_content

    def parse(self):
        try:
            page = fromstring(self.html_content)
            return page
        except TypeError:
            print "Html content not found",
