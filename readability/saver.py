# -*- coding: utf-8 -*-
import os

from abstract import AbstractClass


class FileSaver(AbstractClass):
    content = None
    file_name = None

    def __init__(self, content, config):
        self.set_content(content)
        self.set_config(config)
        self.save()

    def set_content(self, content):
        self.content = content

    def get_content(self):
        return self.content

    def get_filename(self):
        return self.file_name

    def save(self):
        self.file_name = None
