# -*- coding: utf-8 -*-
from abstract import AbstractClass, SetUrlMixinClass
from converter import HtmlConverter as HtC
from formatter import TextFormatter as TeF
from reader import HtmlReader as HtR
from saver import FileSaver as FiS


class Readability(SetUrlMixinClass, AbstractClass):
    def __init__(self, config, url):
        self.set_url(url)
        self.set_config(config)

    def convert(self):
        html_content = HtR(self.get_url()).get_html_content()
        content = HtC(html_content, self.get_config()).get_clear_content()
        formatted = TeF(content, self.get_config()).get_formatted_content()
        return FiS(formatted, self.get_config()).get_filename()
