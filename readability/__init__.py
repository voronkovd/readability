# -*- coding: utf-8 -*-
from readability.read import Readability

__all__ = ['Readability']
