# -*- coding: utf-8 -*-
from abstract import AbstractClass


class TextFormatter(AbstractClass):
    content = None
    formatted_content = None

    def __init__(self, content, config):
        self.set_content(content)
        self.set_config(config)
        self.format()

    def set_content(self, content):
        self.content = content

    def get_content(self):
        return self.content

    def set_formatted_content(self, content):
        self.formatted_content = content

    def get_formatted_content(self):
        return self.formatted_content

    def format(self):
        self.set_formatted_content(self.content)
