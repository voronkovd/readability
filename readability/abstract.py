# -*- coding: utf-8 -*-


class AbstractClass(object):
    config = None

    def get_config(self):
        return self.config

    def set_config(self, config):
        self.config = config


class SetUrlMixinClass(object):
    url = None

    def set_url(self, url):
        self.url = url

    def get_url(self):
        return self.url
