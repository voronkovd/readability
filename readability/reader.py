# -*- coding: utf-8 -*-
import urllib

from abstract import AbstractClass
from abstract import SetUrlMixinClass


class HtmlReader(SetUrlMixinClass, AbstractClass):
    html_content = None

    def __init__(self, url):
        self.set_url(url)
        self.set_html_content()

    def get_html_content(self):
        return self.html_content

    def set_html_content(self):
        try:
            self.html_content = urllib.urlopen(self.get_url()).read()
        except (IOError, AttributeError):
            print "Url is not corrected",
